# Rules

Hi! We hope you're having a really great time on our server! Although we like freedom, we still have to have some rules, otherwise, the server would be a mess.

And we know, mistakes can happen, so don't worry, if it was a true mistake. Lies are transparent though.

Punishments are different. You might just be warned, and you might be banned for a lifetime. And also everything in between and around.

## List

1. Beware of the Discord Terms of Service and Communtiy Guidelines.
2. Do not be racist! I don't think I have to explain why.
3. No swearing! Let's keep this server clean!
4. No promotion! Just don't.
5. Be kind, friendly, respectful, and respectable. Do not be toxic.
6. Do not intentionally annoy people.
7. Do not impersonate anyone. No exception.
8. Speak English! You don't have to be perfect.
9. Do not spam. Only in channels made for spamming.
10. Nothing illegal. Needless to say why.
11. Do not share sensitive content about our bot. What has to be open, is already open.
