# Application

If you wish to apply, first, thank you so much, second, read this and take notes!

You must keep your Discord DMs open for this server so we can contact you.

# Moderator

[OPEN]

## About

This position is for people who would like to help us out and have enough to moderate and can adapt to a few procedures and policies.

# Developer

[OPEN]

## About

Becoming a Developer is not an easy task, but not a hard one either.
Make sure you're at least 12 years old. Exceptions can be made though.
Have an understandable English. Just enough to make sure we understand you.
Have at least 6 free hours a week to work on our bot.

## Requirements

And now, the boring technical stuff...
Since our bot is written in NodeJS using Discord.js, make sure you decently know both of them.
We use Git for version control, make sure you know that.
We use GitLab to collaborate, have a GitLab account, and be familiar with it.

# Tester

[OPEN]

## About

Testers are very much appreciated!

## Requirements

Make sure you have at least 2 hours a week to test.
You will have to review new features and commands.
You should provide us with feedback on what to improve and do.
